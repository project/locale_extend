Locale Extend Modules
=====================

Some experimental modules to try different options as syntactic sugar
for using locale_extend API.

These are just example modules and they cannot support all existing 
languages, so I think we'll just add EU languages... but it is really 
easy to add your own t_LANG, or namespace following the examples.

Locale Extend T
---------------

Using shorthand 't' functions.

> t_es("Hola amigo");
> t_de("Hallo, Freund");

Locale Extend NS
----------------

Using namespaces:

> use function Drupal\locale_extend\es\tr;
>
> tr("Hola");

Want to fully replace 't' function for some module?

> use function Drupal\locale_extend\de\tr as t;
>
> t("Hallo");


    
