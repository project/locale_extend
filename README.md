Locale Extend
=============

This module overrides Drupal Core localization system to allow translations
 **from** different source languages, other than English.

This is a DX module, that allows development teams to build custom modules
using their own language for hardcoded strings.

Features and API
----------------

* It seamlessly extends t() function with a 'srclang' option. Example:

> t("Hola amigo", [], ['srclang' => 'es']);
>

* Extends *User interface translation* form with *Source language* filter and
 field.
    
* It also provides some helper classes. Examples for translating from Spanish:

Static methods, will work for all language codes
>
> use Drupal\locale_extend\Translate;
>
> Translate::translateFromLangcode('es', "Hola");
>
> Translate::es("Hola");
    
A TranslationInterface with a predefined source language.
>
> use Drupal\locale_extend\Translation;
>
> Translation::fromLangcode('es')->t(...);
>
> Translation::fromLangcode('es')->formatPlural(...);

or

> Translation::es()->t(...);
>
> Translation::es()->formatPlural(...);
>
> // All languages are supported with some magic, you can try also
> // Translation::it(), Translation::en_US()...

or

> $t = new Translation("es");
>
> $translated_one = $t("uno");
>
> $translated_two = $t("dos");

Notes for Developers
--------------------

* Since it replaces Drupal core *TranslationManager* service, the extended API can be used
from anywhere, other modules are welcomed to integrate. See also warning below.

* We are extending *locales_source* table for a clean storage. The new field,
 'srclang', defaults to English - en -. Thus storage is fully backwards compatible.
 
* For more syntactic sugar options see submodules:

> // locale_extend_t
>
> t_es("Hola amigo");
>
> t_de("Hallo, Freund");

> // locale_extend_ns
>
> use function Drupal\locale_extend\es\tr;
>
> tr("Hola");

Coming soon
------------

* Some extra support for configuration strings, though I'd rather have other
 module handling that... ?
* An extended *StringTranslationTrait* so it also can be used in classes with a
 predefined language.

 @see the idea here Drupal\locale_extend\StringTranslationTrait;
 
Warning
-------

This module replaces some Drupal Core services thus it cannot be compatible
with other modules doing the same.

@see \Drupal\locale_extend\LocaleExtendServiceProvider

If you are a module maintainer and have a module with such issue, please let me
know, we'll find a way around it...
