<?php

namespace Drupal\locale_extend\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\locale_extend\SourceString;
use Drupal\locale\Form\TranslateEditForm as TranslateEditFormBase;

/**
 * Defines a translation edit form.
 *
 * @internal
 */
class TranslateEditForm extends TranslateEditFormBase {
  use TranslateFormTrait;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $filter_values = $this->translateFilterValues();
    $langcode = $filter_values['langcode'];

    $this->languageManager->reset();
    $languages = $this->languageManager->getLanguages();

    $langname = isset($langcode) ? $languages[$langcode]->getName() : "- None -";

    $form['#attached']['library'][] = 'locale/drupal.locale.admin';

    $form['langcode'] = [
      '#type' => 'value',
      '#value' => $filter_values['langcode'],
    ];

    $form['strings'] = [
      '#type' => 'table',
      '#tree' => TRUE,
      '#language' => $langname,
      '#header' => [
        $this->t('Source string'),
        ['data' => $this->t('Source language'), 'style' => 'width:20em;'],
        $this->t('Translation for @language', ['@language' => $langname]),
      ],
      '#empty' => $this->t('No strings available.'),
      '#attributes' => ['class' => ['locale-translate-edit-table']],
    ];

    if (isset($langcode)) {
      $strings = $this->translateFilterLoadStrings();

      $plurals = $this->getNumberOfPlurals($langcode);

      foreach ($strings as $string) {
        // Cast into source string, will do for our purposes.
        $source = new SourceString($string);
        // Split source to work with plural values.
        $source_array = $source->getPlurals();
        $translation_array = $string->getPlurals();
        if (count($source_array) == 1) {
          // Add original string value and mark as non-plural.
          $plural = FALSE;
          $form['strings'][$string->lid]['original'] = [
            '#type' => 'item',
            '#title' => $this->t('Source string (@language)', ['@language' => $this->t('Built-in English')]),
            '#title_display' => 'invisible',
            '#plain_text' => $source_array[0],
            '#prefix' => '<span lang="en">',
            '#suffix' => '</span>',
          ];
        }
        else {
          // Add original string value and mark as plural.
          $plural = TRUE;
          $original_singular = [
            '#type' => 'item',
            '#title' => $this->t('Singular form'),
            '#plain_text' => $source_array[0],
            '#prefix' => '<span class="visually-hidden">' . $this->t('Source string (@language)', ['@language' => $this->t('Built-in English')]) . '</span><span lang="en">',
            '#suffix' => '</span>',
          ];
          $original_plural = [
            '#type' => 'item',
            '#title' => $this->t('Plural form'),
            '#plain_text' => $source_array[1],
            '#prefix' => '<span lang="en">',
            '#suffix' => '</span>',
          ];
          $form['strings'][$string->lid]['original'] = [
            $original_singular,
            ['#markup' => '<br>'],
            $original_plural,
          ];
        }
        if (!empty($string->context)) {
          $form['strings'][$string->lid]['original'][] = [
            '#type' => 'inline_template',
            '#template' => '<br><small>{{ context_title }}: <span lang="en">{{ context }}</span></small>',
            '#context' => [
              'context_title' => $this->t('In Context'),
              'context' => $string->context,
            ],
          ];
        }

        $form['strings'][$string->lid]['srclang'][] = [
            '#type' => 'item',
            '#title' => $this->t('Source language'),
            '#title_display' => 'invisible',
            '#plain_text' => $string->srclang ? $languages[$string->srclang]->getName() : $this->t("None"),
        ];
        // Approximate the number of rows to use in the default textarea.
        $rows = min(ceil(str_word_count($source_array[0]) / 12), 10);
        if (!$plural) {
          $form['strings'][$string->lid]['translations'][0] = [
            '#type' => 'textarea',
            '#title' => $this->t('Translated string (@language)', ['@language' => $langname]),
            '#title_display' => 'invisible',
            '#rows' => $rows,
            '#default_value' => $translation_array[0],
            '#attributes' => ['lang' => $langcode],
          ];
        }
        else {
          // Add a textarea for each plural variant.
          for ($i = 0; $i < $plurals; $i++) {
            $form['strings'][$string->lid]['translations'][$i] = [
              '#type' => 'textarea',
              // @todo Should use better labels https://www.drupal.org/node/2499639
              '#title' => ($i == 0 ? $this->t('Singular form') : $this->formatPlural($i, 'First plural form', '@count. plural form')),
              '#rows' => $rows,
              '#default_value' => $translation_array[$i] ?? '',
              '#attributes' => ['lang' => $langcode],
              '#prefix' => $i == 0 ? ('<span class="visually-hidden">' . $this->t('Translated string (@language)', ['@language' => $langname]) . '</span>') : '',
            ];
          }
          if ($plurals == 2) {
            // Simplify interface text for the most common case.
            $form['strings'][$string->lid]['translations'][1]['#title'] = $this->t('Plural form');
          }
        }
      }
      if (count(Element::children($form['strings']))) {
        $form['actions'] = ['#type' => 'actions'];
        $form['actions']['submit'] = [
          '#type' => 'submit',
          '#value' => $this->t('Save translations'),
        ];
      }
    }
    $form['pager']['#type'] = 'pager';
    return $form;
  }

}
