<?php

namespace Drupal\locale_extend\Form;

/**
 * Defines the locale user interface translation form base.
 *
 * Provides methods for searching and filtering strings.
 */
trait TranslateFormTrait {

  /**
   * Extends locale translation filters that can be applied.
   */
  protected function translateFilters() {
    // Get all languages, except English.
    // $this->languageManager->reset();
    $filters = parent::translateFilters();
    $languages = $this->languageManager->getLanguages();
    $language_options = [];
    foreach ($languages as $langcode => $language) {
      if (locale_is_translatable($langcode)) {
        $language_options[$langcode] = $language->getName();
      }
    }
    $language_options[''] = $this->t('All');

    $filters['srclang'] = [
        'title' => $this->t('Source language'),
        'options' => $language_options,
        'default' => '',
    ];

    return $filters;
  }

  /**
   * Overrides \Drupal\locale\Form\TranslateFormBase::translateFilterLoadStrings()
   *
   * Need to add in 'srclang' condition.
   *
   * @return \Drupal\locale\TranslationString[]
   *   Array of \Drupal\locale\TranslationString objects.
   */
  protected function translateFilterLoadStrings() {
    $filter_values = $this->translateFilterValues();

    // Language is sanitized to be one of the possible options in
    // translateFilterValues().
    $conditions = ['language' => $filter_values['langcode']];
    $options = ['pager limit' => 30, 'translated' => TRUE, 'untranslated' => TRUE];

    // Add in source language.
    if ($filter_values['srclang']) {
      $conditions['srclang'] = $filter_values['srclang'];
    }
    // Add translation status conditions and options.
    switch ($filter_values['translation']) {
      case 'translated':
        $conditions['translated'] = TRUE;
        if ($filter_values['customized'] != 'all') {
          $conditions['customized'] = $filter_values['customized'];
        }
        break;

      case 'untranslated':
        $conditions['translated'] = FALSE;
        break;

    }

    if (!empty($filter_values['string'])) {
      $options['filters']['source'] = $filter_values['string'];
      if ($options['translated']) {
        $options['filters']['translation'] = $filter_values['string'];
      }
    }

    return $this->localeStorage->getTranslations($conditions, $options);
  }

}
