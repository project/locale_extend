<?php

namespace Drupal\locale_extend\Form;

use Drupal\locale\Form\TranslateFilterForm as TranslateFilterFormFormBase;

/**
 * Provides a filtered translation edit form.
 *
 * Extends locale edit form just to add-in some overrides with the trait.
 *
 * @internal
 */
class TranslateFilterForm extends TranslateFilterFormFormBase {
  use TranslateFormTrait;
}
