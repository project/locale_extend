<?php

namespace Drupal\locale_extend;

use Drupal\locale\SourceString as SourceStringBase;

/**
 * Extends the locale source string object to add different languages.
 */
class SourceString extends SourceStringBase {

  /**
   * The language code.
   *
   * Defaults to English.
   *
   * @var string
   */
  public $srclang = 'en';

}
