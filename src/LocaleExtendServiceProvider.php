<?php

namespace Drupal\locale_extend;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Replaces some locale / translation services.
 */
class LocaleExtendServiceProvider extends ServiceProviderBase {

  public function alter(ContainerBuilder $container) {
    // Replace Drupal core TranslationManager
    $container->getDefinition('string_translation')
      ->setClass('Drupal\locale_extend\Service\TranslationManager');
    // Replace some locale services
    $container->getDefinition('locale.storage')
      ->setClass('Drupal\locale_extend\Service\ExtendedStringDatabaseStorage');
    $container->getDefinition('string_translator.locale.lookup')
      ->setClass('Drupal\locale_extend\Service\ExtendedLocaleTranslation');
  }

}
