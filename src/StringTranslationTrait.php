<?php

namespace Drupal\locale_extend;

use Drupal\Core\StringTranslation\StringTranslationTrait as BaseTrait;

/**
 * Wrapper methods for \Drupal\Core\StringTranslation\TranslationInterface.
 *
 * Extended StringTranslationTrait to support a predefind source language.
 *
 * @todo EXPERIMENTAL
 *
 * @usage
 *
 *   use Drupal\locale_extend\StringTranslationTrait;
 *
 *   class MyClass {
 *     use StringTranslationTrait;
 *     // Source language for all string translations in this class.
 *     protected $source_langcode = 'de';
 *
 *     function hello() {
 *        // This will assume source language is German
 *        return $this->t("Hallo");
 *     }
 *   }
 *
 * @ingroup i18n
 */
trait StringTranslationTrait {
  use BaseTrait;

  /**
   * The source language code.
   *
   * @var string
   */
  protected $source_langcode;

  /**
   * {@inheritdoc}
   */
  protected function t($string, array $args = [], array $options = []) {
    $options + ['srclang' => $this->source_langcode];
    return BaseTrait::t($string, $args, $options);
  }

  /**
   * Formats a string containing a count of items.
   *
   * @see \Drupal\Core\StringTranslation\TranslationInterface::formatPlural()
   */
  protected function formatPlural($count, $singular, $plural, array $args = [], array $options = []) {
    $options + ['srclang' => $this->source_langcode];
    return BaseTrait::formatPlural($count, $singular, $plural, $args, $options);
  }

}
