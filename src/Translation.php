<?php

namespace Drupal\locale_extend;

use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a TranslationInterface with predefined source language.
 *
 * Usage. To translate from Spanish, use either:
 *
 *   Translation::fromLangcode('es')->t(...);
 *
 *   Translation::fromLangcode('es')->formatPlural(...);
 *
 * or
 *
 *   Translation::es()->t(...);
 *   Translation::es()->formatPlural(...);
 *
 *   // All languages are supported with some magic, try also: Translation::it(), Translation::en_US()...
 *
 * or
 *
 *   $t = new Translation("es");
 *
 *   $translated_one = $t("uno");
 *   $translated_two = $t("dos");
 *
 * or, more advanced: Translate to German, reuse context
 *
 *   $t = new Translation("es", [], ['langcode' => 'de', 'context' => 'Numbers']);
 *
 *   $translated_one = $t("uno");
 *   $translated_two = $t("dos");
 */
class Translation implements TranslationInterface {

  /**
   * Language code to translate from.
   *
   * @var string $source_langcode
   */
  protected $source_langcode;

  /**
   * An associative array of replacements, to be reused.
   *
   * @var array
   */
  protected $args = [];

  /**
   * An associative array of additional options, to be reused.
   *
   * @var array
   */
  protected $options = [];

  /**
   * The string translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * Creates a new Translation object.
   *
   * @param string $source_langcode
   *    Language code to translate from
   * @param array $args
   *   (optional) An associative array of replacements.
   * @param array $options
   *   (optional) An associative array of additional options.
   */
  public function __construct(string $source_langcode, array $args = [], array $options = []) {
    $this->source_langcode = $source_langcode;
  }

  /**
   * Creates a new Translation object.
   *
   * @param string $source_langcode
   *    Language code to translate from
   * @param array $args
   *   (optional) An associative array of replacements.
   * @param array $options
   *   (optional) An associative array of additional options.
   *
   * @see t().
   */
  public static function fromLangcode(string $source_langcode, array $args = [], array $options = []) {
    return new static($source_langcode, $args, $options);
  }

  /**
   * Magic create new Translation object using language from method name.
   *
   * The method name is the language code. Use like:
   *
   *   Translation::es()
   *   Translation::de()
   *   ....
   */
  public static function __callStatic($name, $arguments) {
    return static::fromLangcode($name);
  }

  /**
   * Makes object callable...
   *
   * Example usage:
   *
   *   $translated_strings = array_map($source_strings, new Translation('es'));
   */
  public function __invoke($string, array $args = [], array $options = []) {
    return $this->translate($string, $args, $options);
  }

  /**
   * Helper method to just translate string
   *
   * @see t()
   */
  public function t($string, array $args = [], array $options = []) {
    return $this->translate($string, $args, $options);
  }

  /**
   * Gets the string translation service.
   *
   * @return \Drupal\Core\StringTranslation\TranslationInterface
   *   The string translation service.
   */
  protected function getStringTranslation() {
    if (!$this->stringTranslation) {
      $this->stringTranslation = \Drupal::service('string_translation');
    }
    return $this->stringTranslation;
  }

  /**
   * Sets the string translation service to use.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation
   *   The string translation service.
   *
   * @return $this
   */
  public function setStringTranslation(TranslationInterface $translation) {
    $this->stringTranslation = $translation;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function translate($string, array $args = [], array $options = []) {
    // Add predefined args and options.
    $args += $this->args;
    $options += ['srclang' => $this->source_langcode] + $this->options;

    return new TranslatableMarkup($string, $args, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function translateString(TranslatableMarkup $translated_string) {
    return $this->getStringTranslation()->translateString($translated_string);
  }

  /**
   * {@inheritdoc}
   */
   public function formatPlural($count, $singular, $plural, array $args = [], array $options = []) {
     // Add predefined args and options.
     $args += $this->args;
     $options += ['srclang' => $this->source_langcode] + $this->options;

     return \Drupal::translation()->formatPlural($count, $singular, $plural, $args, $options);
   }
}