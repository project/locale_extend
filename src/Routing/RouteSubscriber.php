<?php
namespace Drupal\locale_extend\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Change path '/admin/config/regional/translate'
    if ($route = $collection->get('locale.translate_page')) {
          $route->addDefaults([
              '_controller' => '\Drupal\locale_extend\Controller\LocaleController::translatePage',
              '_title' => 'User interface translation extended',
          ]);
    }
  }

}