<?php

namespace Drupal\locale_extend\Service;

use Drupal\Component\Gettext\PoItem;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\locale\LocaleLookup;
use Drupal\locale\StringStorageInterface;

/**
 * A cache collector to allow for dynamic building of the locale cache.
 */
class ExtendedLocaleLookup extends LocaleLookup {

  /**
   * The source language code.
   *
   * @var string
   */
  protected $source_langcode;

  /**
   * Constructs a locale_extendLookup object.
   *
   * @param string $target_langcode
   *   The language code to translate to.
   * @param string $source_langcode
   *   The source language code.
   * @param string $context
   *   The string context.
   * @param \Drupal\locale\StringStorageInterface $string_storage
   *   The string storage.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock backend.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct($target_langcode, $source_langcode, $context, StringStorageInterface $string_storage, CacheBackendInterface $cache, LockBackendInterface $lock, ConfigFactoryInterface $config_factory, LanguageManagerInterface $language_manager, RequestStack $request_stack) {
    parent::__construct($target_langcode, $context, $string_storage, $cache, $lock, $config_factory, $language_manager, $request_stack);
    $this->source_langcode = $source_langcode;
  }

  /**
   * {@inheritdoc}
   */
  protected function getCid() {
    if (!isset($this->cid)) {
      // Add the current user's role IDs to the cache key, this ensures that,
      // for example, strings for admin menu items and settings forms are not
      // cached for anonymous users.
      $user = \Drupal::currentUser();
      $rids = '';
      if ($user) {
        $roles = $user->getRoles();
        sort($roles);
        $rids = implode(':', $roles);
      }
      $this->cid = "locale:{$this->source_langcode}:{$this->langcode}:{$this->context}:$rids";

      // Getting the roles from the current user might have resulted in t()
      // calls that attempted to get translations from the locale cache. In that
      // case they would not go into this method again as
      // CacheCollector::lazyLoadCache() already set the loaded flag. They would
      // however call resolveCacheMiss() and add that string to the list of
      // cache misses that need to be written into the cache. Prevent that by
      // resetting that list. All that happens in such a case are a few uncached
      // translation lookups.
      $this->keysToPersist = [];
    }
    return $this->cid;
  }

  /**
   * {@inheritdoc}
   */
  protected function resolveCacheMiss($offset) {
    $translation = $this->stringStorage->findTranslation([
      'language' => $this->langcode,
      'source' => $offset,
      'srclang' => $this->source_langcode,
      'context' => $this->context,
    ]);

    if ($translation) {
      $value = !empty($translation->translation) ? $translation->translation : TRUE;
    }
    else {
      // We don't have the source string, update the {locales_source} table to
      // indicate the string is not translated.
      $this->stringStorage->createString([
        'source' => $offset,
        'srclang' => $this->source_langcode,
        'context' => $this->context,
        'version' => \Drupal::VERSION,
      ])->addLocation('path', $this->requestStack->getCurrentRequest()->getRequestUri())->save();
      $value = TRUE;
    }

    // If there is no translation available for the current language then use
    // language fallback to try other translations.
    if ($value === TRUE) {
      $fallbacks = $this->languageManager->getFallbackCandidates(['langcode' => $this->langcode, 'operation' => 'locale_lookup', 'data' => $offset]);
      if (!empty($fallbacks)) {
        foreach ($fallbacks as $langcode) {
          $translation = $this->stringStorage->findTranslation([
            'language' => $langcode,
            'source' => $offset,
            'srclang' => $this->source_langcode,
            'context' => $this->context,
          ]);

          if ($translation && !empty($translation->translation)) {
            $value = $translation->translation;
            break;
          }
        }
      }
    }

    if (is_string($value) && strpos($value, PoItem::DELIMITER) !== FALSE) {
      // Community translations imported from localize.drupal.org as well as
      // migrated translations may contain @count[number].
      $value = preg_replace('!@count\[\d+\]!', '@count', $value);
    }

    $this->storage[$offset] = $value;
    // Disabling the usage of string caching allows a module to watch for
    // the exact list of strings used on a page. From a performance
    // perspective that is a really bad idea, so we have no user
    // interface for this. Be careful when turning this option off!
    if ($this->configFactory->get('locale.settings')->get('cache_strings')) {
      $this->persist($offset);
    }
    return $value;
  }

}
