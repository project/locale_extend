<?php

namespace Drupal\locale_extend\Service;

use Drupal\Core\DestructableInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\locale\LocaleTranslation as LocaleTranslationBase;

/**
 * Extended String translator using the locale_extend module.
 */
class ExtendedLocaleTranslation extends LocaleTranslationBase implements ExtendedTranslatorInterface {

  /**
   * {@inheritdoc}
   */
  public function getStringTranslation($langcode, $string, $context, array $options = []) {
    $source_langcode = !empty($options['srclang']) ? $options['srclang'] : ExtendedTranslatorInterface::DEFAULT_SRCLANG;

    // Check whether we can translate these specific languages.
    if (!$this->canTranslateLanguages($source_langcode, $langcode)) {
      return FALSE;
    }

    // Strings are cached by source langcode, target langcode, context and roles, using instances of the
    // LocaleLookup class to handle string lookup and caching.
    if (!isset($this->translations[$source_langcode][$langcode][$context])) {
      $this->translations[$source_langcode][$langcode][$context] = new ExtendedLocaleLookup($langcode, $source_langcode, $context, $this->storage, $this->cache, $this->lock, $this->configFactory, $this->languageManager, $this->requestStack);
    }
    $translation = $this->translations[$source_langcode][$langcode][$context]->get($string);
    return $translation === TRUE ? FALSE : $translation;
  }

  /**
   * {@inheritdoc}
   */
  public function canTranslateLanguages($from_langcode, $to_langcode) {
    // If the language is not suitable for locale module, just return.
    if ($from_langcode == LanguageInterface::LANGCODE_SYSTEM) {
      return FALSE;
    }
    elseif ($from_langcode == $to_langcode) {
      // Only if English, maybe...
      return $from_langcode == 'en' && $this->canTranslateEnglish();
    }
    else {
      // Yes, we can translate from any language to any different one.
      return TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function destruct() {
    foreach ($this->translations as $language_translations) {
      foreach ($language_translations as $context) {
        foreach ($context as $lookup) {
          if ($lookup instanceof DestructableInterface) {
            $lookup->destruct();
          }
        }
      }
    }
  }
}
