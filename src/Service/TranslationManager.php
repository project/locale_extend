<?php

namespace Drupal\locale_extend\Service;

use Drupal\Core\StringTranslation\TranslationManager as TranslationManagerBase;

/**
 * Extends Drupal Core TranslationManager to support different source languages.
 */
class TranslationManager extends TranslationManagerBase implements ExtendedTranslatorInterface {

  /**
   * {@inheritdoc}
   */
  public function canTranslateLanguages($from_langcode, $to_langcode) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getStringTranslation($langcode, $string, $context, array $options = []) {
    $source_langcode = !empty($options['srclang']) ? $options['srclang'] : ExtendedTranslatorInterface::DEFAULT_SRCLANG;
    $default_srclang = $source_langcode == ExtendedTranslatorInterface::DEFAULT_SRCLANG;

    if ($this->sortedTranslators === NULL) {
      $this->sortedTranslators = $this->sortTranslators();
    }
    $translation = FALSE;
    foreach ($this->sortedTranslators as $translator) {
      if ($default_srclang) {
        $translation = $translator->getStringTranslation($langcode, $string, $context);
      }
      elseif ($translator instanceof ExtendedTranslatorInterface) {
        // Strings having source langcode can only be translated with the right translator.
        $translation = $translator->getStringTranslation($langcode, $string, $context, $options);
      }
      if ($translation !== FALSE) {
        return $translation;
      }
    }
    // No translator got a translation.
    return FALSE;
  }

  /**
   * Translates a string to the current language or to a given language.
   *
   * @param string $string
   *   A string containing the English text to translate.
   * @param array $options
   *   An associative array of additional options, with the following elements:
   *   - 'langcode': The language code to translate to a language other than
   *      what is used to display the page.
   *   - 'context': The context the source string belongs to.
   *
   * @return string
   *   The translated string.
   */
  protected function doTranslate($string, array $options = []) {
    // If a NULL langcode has been provided, unset it.
    if (!isset($options['langcode']) && array_key_exists('langcode', $options)) {
      unset($options['langcode']);
    }

    // Merge in options defaults.
    $options = $options + [
      'langcode' => $this->defaultLangcode,
      'context' => '',
      'srclang' => ExtendedTranslatorInterface::DEFAULT_SRCLANG,
    ];

    $translation = $this->getStringTranslation($options['langcode'], $string, $options['context'], $options);
    return $translation === FALSE ? $string : $translation;
  }

}
