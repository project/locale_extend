<?php

namespace Drupal\locale_extend\Service;

use Drupal\locale\StringDatabaseStorage;
use Drupal\locale\StringStorageException;
use Drupal\locale_extend\SourceString;

/**
 * Extends Core storage to allow different source languages.
 */
class ExtendedStringDatabaseStorage extends StringDatabaseStorage {

  /**
   * {@inheritdoc}
   */
  public function getStrings(array $conditions = [], array $options = []) {
    return $this->dbStringLoad($conditions, $options, 'Drupal\locale_extend\SourceString');
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslations(array $conditions = [], array $options = []) {
    return $this->dbStringLoad($conditions, ['translation' => TRUE] + $options, 'Drupal\locale_extend\TranslationString');
  }

  /**
   * {@inheritdoc}
   */
  public function findString(array $conditions) {
    $values = $this->dbStringSelect($conditions)
    ->execute()
    ->fetchAssoc();

    if (!empty($values)) {
      $string = new SourceString($values);
      $string->setStorage($this);
      return $string;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createString($values = []) {
    return new SourceString($values + ['storage' => $this]);
  }

  /**
   * Creates a database record for a string object.
   *
   * @param \Drupal\locale\StringInterface $string
   *   The string object.
   *
   * @return bool|int
   *   If the operation failed, returns FALSE.
   *   If it succeeded returns the last insert ID of the query, if one exists.
   *
   * @throws \Drupal\locale\StringStorageException
   *   If the string is not suitable for this storage, an exception is thrown.
   */
  protected function dbStringInsert($string) {
    if ($string->isSource()) {
      $string->setValues(['context' => '', 'version' => 'none'], FALSE);
      $fields = $string->getValues(['source', 'srclang', 'context', 'version']);
    }
    elseif ($string->isTranslation()) {
      $string->setValues(['customized' => 0], FALSE);
      $fields = $string->getValues(['lid', 'language', 'translation', 'customized']);
    }
    if (!empty($fields)) {
      return $this->connection->insert($this->dbStringTable($string), $this->options)
        ->fields($fields)
        ->execute();
    }
    else {
      throw new StringStorageException('The string cannot be saved: ' . $string->getString());
    }
  }

  /**
   * Updates string object in the database.
   *
   * @param \Drupal\locale\StringInterface $string
   *   The string object.
   *
   * @return bool|int
   *   If the record update failed, returns FALSE. If it succeeded, returns
   *   SAVED_NEW or SAVED_UPDATED.
   *
   * @throws \Drupal\locale\StringStorageException
   *   If the string is not suitable for this storage, an exception is thrown.
   */
  protected function dbStringUpdate($string) {
    if ($string->isSource()) {
      $values = $string->getValues(['source', 'srclang', 'context', 'version']);
    }
    elseif ($string->isTranslation()) {
      $values = $string->getValues(['translation', 'customized']);
    }
    if (!empty($values) && $keys = $this->dbStringKeys($string)) {
      return $this->connection->merge($this->dbStringTable($string), $this->options)
        ->keys($keys)
        ->fields($values)
        ->execute();
    }
    else {
      throw new StringStorageException('The string cannot be updated: ' . $string->getString());
    }
  }

}
