<?php

namespace Drupal\locale_extend\Service;

/**
 * Interface for objects capable of string translation with extra options.
 */
interface ExtendedTranslatorInterface {
  /**
   * The default source language. English, of course.
   */
  const DEFAULT_SRCLANG = 'en';

  /**
   * Retrieves English string to given language with extra options
   *
   * @param string $langcode
   *   Language code to translate to.
   * @param string $string
   *   The source string.
   * @param string $context
   *   The string context.
   * @param array $options
   *   Optional aditional translation options.
   *   - 'srclang' => Source language.
   *
   * @return string|false
   *   Translated string if there is a translation, FALSE if not.
   */
  public function getStringTranslation($langcode, $string, $context, array $options = []);

  /**
   * Whether this translator can translate.
   *
   * @param string $from_langcode
   * @param string $to_langcode
   *
   * @return boolean
   *   TRUE if the translator can translate these languages.
   */
  public function canTranslateLanguages($from_langcode, $to_langcode);

}
