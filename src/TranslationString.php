<?php

namespace Drupal\locale_extend;

use Drupal\locale\TranslationString as TranslationStringBase;

/**
 * Extends the locale source string object to add different languages.
 */
class TranslationString extends TranslationStringBase {

  /**
   * The language code.
   *
   * Defaults to English.
   *
   * @var string
   */
  public $srclang = 'en';

}
