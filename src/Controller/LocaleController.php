<?php

namespace Drupal\locale_extend\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Return response for manual check translations.
 */
class LocaleController extends ControllerBase {

  /**
   * Shows the string search screen.
   *
   * @return array
   *   The render array for the string search screen.
   */
  public function translatePage() {
    return [
      'filter' => $this->formBuilder()->getForm('Drupal\locale_extend\Form\TranslateFilterForm'),
      'form' => $this->formBuilder()->getForm('Drupal\locale_extend\Form\TranslateEditForm'),
    ];
  }

}
