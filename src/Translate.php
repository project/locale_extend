<?php

namespace Drupal\locale_extend;

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Helper static methods for translation.
 *
 * Usage. To translate from Spanish, use either:
 *
 *   Translate::translateFromLangcode('es', "Hola");
 *
 * or
 *
 *   Translate::es("Hola");
 *
 */
class Translate {

  /**
   * Translates a string to the current language or to a given language.
   *
   * @param string $source_langcode
   *    Language code to translate from
   * @param string $string
   *   A string containing the English text to translate.
   * @param array $args
   *   (optional) An associative array of replacements to make after translation.
   *   Based on the first character of the key, the value is escaped and/or
   *   themed. See
   *   \Drupal\Component\Render\FormattableMarkup::placeholderFormat() for
   *   details.
   * @param array $options
   *   (optional) An associative array of additional options, with the following
   *   elements:
   *   - 'langcode' (defaults to the current language): A language code, to
   *     translate to a language other than what is used to display the page.
   *   - 'context' (defaults to the empty context): The context the source string
   *     belongs to. See the @link i18n Internationalization topic @endlink for
   *     more information about string contexts.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   An object that, when cast to a string, returns the translated string.
   *
   * @see t()
   *
   * @ingroup sanitization
   */
  public static function translateFromLangcode(string $source_langcode, $string, array $args = [], array $options = []) {
    $options += ['srclang' => $source_langcode];
    return new TranslatableMarkup($string, $args, $options);
  }

  /**
   * Gets the string translation service with a predefined source language.
   *
   * This is a shorthand function to get a Translation object.
   *
   * @see \Drupal\locale_extend\Translation
   *
   * @param string $source_langcode
   *    Language code to translate from
   *
   * @return \Drupal\Core\StringTranslation\TranslationInterface
   *   A string translation service with predefined source language.
   */
  public static function translation($source_langcode) {
    return Translation::fromLangcode($source_langcode);
  }

  /**
   * Magic translation happens here
   *
   * The method name is the language code. Use like:
   *
   *   Translate::es($string)
   *   Translate::de($string)
   *   ....
   */
  public static function __callStatic($name, $arguments) {
    list($string, $args, $options) = array_merge($arguments, [[], []]);
    return static::translatefromLangcode($name, $string, $args, $options);
  }
}