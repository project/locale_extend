<?php

namespace Drupal\Tests\locale_extend\Kernel;

use Drupal\Tests\locale\Kernel\LocaleStringTest as LocaleTestBase;

/**
 * Tests the locale string storage, string objects and data API
 * with locale_extend enabled.
 *
 * @group locale_extend
 */
trait LocaleExtendKernelTestTrait  {

  /**
   * Adds 'srclang' field to locale schema.
   */
  protected function extendSchema(): void {
    \Drupal::moduleHandler()->loadInclude('locale_extend', 'install');
    $schema = $this->container->get('database')->schema();
    locale_extend_install_schema_add_fields($schema);
  }

}

