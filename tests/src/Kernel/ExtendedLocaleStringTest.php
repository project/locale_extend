<?php

namespace Drupal\Tests\locale_extend\Kernel;

use Drupal\Tests\locale\Kernel\LocaleStringTest;

/**
 * Tests the locale string storage, string objects and data API
 * with different source languages.
 *
 * @group locale_extend
 */
class ExtendedLocaleStringTest extends LocaleStringTest {
  use LocaleExtendKernelTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'language',
    'locale',
    'locale_extend'
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->extendSchema();
  }

  /**
   * Tests CRUD API.
   */
  public function testStringCrudApi() {
    // Create source strings in different languages.
    $langcodes = ['en', 'de', 'es'];
    $sources = [];
    foreach ($langcodes as $srclang) {
      $source = $this->buildSourceString(['srclang' => $srclang])->save();
      $this->assertNotEmpty($source->lid);
      $this->assertEquals($source->srclang, $srclang);
      $sources[$srclang] = $source;
      // Load strings by lid, source and srclang.
      $string1 = $this->storage->findString(['srclang' => $srclang]);
      $this->assertEquals($source, $string1);
      $string2 = $this->storage->findString(['source' => $source->source, 'srclang' => $srclang]);
      $this->assertEquals($source, $string2);
      $string3 = $this->storage->findString(['source' => $source->source, 'srclang' => 'xx']);
      $this->assertNull($string3);
    }

    // Create translation and find it by lid, source and srclang.
    foreach ($langcodes as $srclang) {
      $source = $sources[$srclang];
      $translation_langs = array_diff($langcodes, [$srclang]);

      foreach ($translation_langs as $langcode) {
        $translation = $this->createTranslation($source, $langcode);
        $this->assertEquals(LOCALE_NOT_CUSTOMIZED, $translation->customized);
        $string1 = $this->storage->findTranslation(['language' => $langcode, 'lid' => $source->lid]);
        $this->assertEquals($translation->translation, $string1->translation);

        $string2 = $this->storage->findTranslation(['language' => $langcode, 'source' => $source->source, 'srclang' => $srclang]);
        $this->assertEquals($translation->translation, $string2->translation);

        // Delete translation.
        $translation->delete();
        $deleted = $this->storage->findTranslation(['language' => $langcode, 'lid' => $source->lid]);
        $this->assertNull($deleted->translation);
      }
    }

  }

  /**
   * Tests Search API loading multiple objects with multiple source langs.
   */
  public function testStringSearchApi() {
    $langcodes = ['en', 'de', 'es'];
    $language_count = 3;
    // Strings 1 and 2 will have some common prefix.
    // Source 1 will have all translations, not customized.
    // Source 2 will have all translations, customized.
    // Source 3 will have no translations.
    $prefix = $this->randomMachineName(100);

    // Keep count of source strings as we add each language.
    // We will end up with
    $count = 0;
    foreach ($langcodes as $srclang) {
      $count++;
      $source1 = $this->buildSourceString(['srclang' => $srclang, 'source' => $prefix . $this->randomMachineName(100)])->save();
      $source2 = $this->buildSourceString(['srclang' => $srclang, 'source' => $prefix . $this->randomMachineName(100)])->save();
      $source3 = $this->buildSourceString(['srclang' => $srclang])->save();


      // Load all source strings with this langcode.
      $strings = $this->storage->getStrings(['srclang' => $srclang]);
      $this->assertCount(3, $strings);

      // Load all source strings for al source langs.
      $strings = $this->storage->getStrings([]);
      $this->assertCount(3 * $count, $strings);

      // Load all source strings matching a given string.
      $filter_options['filters'] = ['source' => $prefix];
      $strings = $this->storage->getStrings([], $filter_options);
      $this->assertCount(2 * $count, $strings);

      // Load all source strings matching a given string filtering with srclang.
      $filter_options['filters'] = ['source' => $prefix];
      $strings = $this->storage->getStrings(['srclang' => $srclang], $filter_options);
      $this->assertCount(2, $strings);

      // Create translations for first source
      $translate1 = $this->createAllTranslations($source1);
    }
    // Try quick search function with different field combinations.
    // Note latest string sources are Spanish
    $langcode = 'de';
    $found = $this->storage->findTranslation(['language' => $langcode, 'source' => $source1->source, 'context' => $source1->context]);
    $this->assertNotNull($found, 'Translation not found searching by source and context.');
    $this->assertNotNull($found->language);
    $this->assertNotNull($found->translation);
    $this->assertFalse($found->isNew());
    $this->assertEquals($translate1[$langcode]->translation, $found->translation);

    // Now try a translation not found.
    $found = $this->storage->findTranslation(['language' => $langcode, 'source' => $source3->source, 'context' => $source3->context]);
    $this->assertNotNull($found);
    $this->assertSame($source3->lid, $found->lid);
    $this->assertNull($found->translation);
    $this->assertTrue($found->isNew());

    // Load all translations. For next queries we'll be loading only translated
    // strings.
    $translations = $this->storage->getTranslations(['translated' => TRUE]);
    $this->assertCount($language_count * $language_count, $translations);

    // Load all Spanish translations.
    $translations = $this->storage->getTranslations(['language' => 'es', 'translated' => TRUE]);
    $this->assertCount($language_count, $translations);

    // Load all source strings without translation (1).
    $translations = $this->storage->getStrings(['translated' => FALSE]);
    $this->assertCount(2* $language_count, $translations);

    // Load Spanish translations using string filter.
    $filter_options['filters'] = ['source' => $prefix];
    $translations = $this->storage->getTranslations(['language' => 'es'], $filter_options);
    $this->assertCount(2 * $language_count, $translations);
  }

}
