<?php

namespace Drupal\Tests\locale_extend\Kernel;

use Drupal\Tests\locale\Kernel\LocaleStringTest as LocaleStringTestBase;

/**
 * Tests the locale string storage, string objects and data API
 * with locale_extend enabled.
 *
 * @group locale_extend
 */
class LocaleStringTest extends LocaleStringTestBase {
  use LocaleExtendKernelTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
      'language',
      'locale',
      'locale_extend',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->extendSchema();
  }

  /**
   * Tests CRUD API with Locale Extend enabled.
   */
  public function testStringCrudApi() {
    parent::testStringCrudApi();
  }

  /**
   * Tests Search API with Locale Extend enabled.
   */
  public function testStringSearchApi() {
    parent::testStringSearchApi();
  }

}
